#!/bin/bash

# set -e

coverage run manage.py test django_wireguard $@
coverage html -d /artifacts
