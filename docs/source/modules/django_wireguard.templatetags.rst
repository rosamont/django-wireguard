django\_wireguard.templatetags package
======================================

Submodules
----------

django\_wireguard.templatetags.django\_wireguard\_filters module
----------------------------------------------------------------

.. automodule:: django_wireguard.templatetags.django_wireguard_filters
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard.templatetags
   :members:
   :undoc-members:
   :show-inheritance:
