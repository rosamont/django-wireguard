django\_wireguard.wagtail package
=================================

Submodules
----------

django\_wireguard.wagtail.admin module
--------------------------------------

.. automodule:: django_wireguard.wagtail.admin
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard.wagtail
   :members:
   :undoc-members:
   :show-inheritance:
