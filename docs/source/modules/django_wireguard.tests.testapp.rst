django\_wireguard.tests.testapp package
=======================================

Submodules
----------

django\_wireguard.tests.testapp.settings module
-----------------------------------------------

.. automodule:: django_wireguard.tests.testapp.settings
   :members:
   :undoc-members:
   :show-inheritance:

django\_wireguard.tests.testapp.urls module
-------------------------------------------

.. automodule:: django_wireguard.tests.testapp.urls
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_wireguard.tests.testapp
   :members:
   :undoc-members:
   :show-inheritance:
