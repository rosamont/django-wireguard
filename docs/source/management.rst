===================
Management Commands
===================

``django_wireguard`` offers some utility management commands to manage the WireGuard network from the command line:

Interface Creation/Update
=========================

To create or update a WireGuard interface, use the ``setup_interface`` command:

.. code-block::

   python manage.py setup_interface interfaceName \
                                    --listen-port 12345 \
                                    --address 10.100.20.1/32 [..] \
                                    --private-key [base64 private key]

If ``interfaceName`` exists, it will be updated with the provided values.

Peer Creation/Deletion
======================

To create a new peer use the ``create_peer`` command.
Checkout the command options using

.. code-block::

   python manage.py create_peer -h


To delete peers use the ``delete_peer`` command providing the public key(s) of the peer(s) to delete.

.. code-block::

   python manage.py delete_peer <peer's public key> [..]

To delete all peers:

.. code-block::

   python manage.py delete_peer --all

Private Keys Cleanup
====================

Peer's auto-generate private keys will be stored in the database, unless the ``WIREGUARD_STORE_PRIVATE_KEYS`` setting is False.

To clear the database from peer private keys, run

.. code-block::

   python manage.py clear_private_keys

.. note:: Only peer private keys will be cleared.
   Interface private keys need to be stored to re-enable the interface upon restart.
