================
Django Wireguard
================

This is a Django app that provides management via Admin Site for WireGuard interfaces and peers.

.. toctree::
   :maxdepth: 2

   installation
   signals
   management
   modules


Installation
============

.. note:: Make sure you have WireGuard's kernel module installed and loaded.
          Checkout the `official WireGuard website <https://www.wireguard.com/install/>`_ for more information.

Install with pip: ``pip install django-wireguard``


Quick start
-----------

1. Add "django_wireguard" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'django_wireguard',
    ]

2. Run ``python manage.py migrate`` to create the models.

3. Visit http://localhost:8000/admin/ to manage the VPN. Note: you must enable the Django Admin Site first https://docs.djangoproject.com/en/3.1/ref/contrib/admin/.


Enabling the Wagtail Integration
--------------------------------

1. Add "django_wireguard.wagtail" to your INSTALLED_APPS setting after simple_vpn::

    INSTALLED_APPS = [
        ...
        'django_wireguard'
        'django_wireguard.wagtail',
    ]

2. You can manage the VPN from the Wagtail Admin Panel Settings. ``Inspect`` a WireguardPeer object to view their configuration.


Testing with Docker
===================

1. Build the test image by running::

    docker build -f Dockerfile.test -t django_wg_test .


2. Run the tests

   - To run all tests, including WireGuard integration tests:
      1. Make sure the WireGuard kernel modules are installed and loaded on the host machine
      2. Run all tests with NET_ADMIN capability enabled::

            docker run --cap-add NET_ADMIN django_wg_test

   - To run unit tests without WireGuard support::

        docker run django_wg_test --exclude-tag net


