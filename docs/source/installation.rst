============
Installation
============

.. note:: Make sure you have WireGuard's kernel module installed and loaded.
          Checkout the `official WireGuard website <https://www.wireguard.com/install/>`_ for more information.

Install with pip: ``pip install django-wireguard``


Quick start
-----------

1. Add "django_wireguard" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'django_wireguard',
    ]

2. Run ``python manage.py migrate`` to create the models.

3. Visit http://localhost:8000/admin/ to manage the VPN. Note: you must enable the Django Admin Site first https://docs.djangoproject.com/en/3.1/ref/contrib/admin/.


Enabling the Wagtail Integration
--------------------------------

1. Add "django_wireguard.wagtail" to your INSTALLED_APPS setting after simple_vpn::

    INSTALLED_APPS = [
        ...
        'django_wireguard'
        'django_wireguard.wagtail',
    ]

2. You can manage the VPN from the Wagtail Admin Panel Settings. ``Inspect`` a WireguardPeer object to view their configuration.


Configuration
-------------

The following settings can be provided:

* ``WIREGUARD_ENDPOINT`` the endpoint for the peer configuration. Set it to the server Public IP address or domain. Default: ``localhost``.
* ``WIREGUARD_STORE_PRIVATE_KEYS`` set this to False to disable auto generation of peer private keys. Default: ``True``.
* ``WIREGUARD_WAGTAIL_SHOW_IN_SETTINGS`` set this to False to show WireGuard models in root sidebar instead of settings panel. Default: ``True``.

.. topic:: WireGuard Interface PostUp/PostDown bindings

   This package implements `signals <https://docs.djangoproject.com/en/3.1/topics/signals/>`_ as substitute for the PostUp/PostDown directives
   when WireGuard interfaces are set up or down.
   `Read more about django_wireguard signals. <signals.html>`_


