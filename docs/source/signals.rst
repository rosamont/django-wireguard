=======
Signals
=======

When a :class:`~django_wireguard.models.WireguardInterface` object is created or deleted, it will automatically send out the
standard Django signals (``pre_save``, ``post_save``, ``pre_delete``, ``post_delete``).

``django_wireguard`` adds to them a couple more signals:

Creation (PostUp equivalent)
============================

:mod:`~django_wireguard.signals.interface_created`
Sent whenever a WireGuard interface (**not** database object) is created (gets up).

Deletion (PostDown equivalent)
==============================

:mod:`~django_wireguard.signals.interface_deleted`
Sent whenever a WireGuard interface (**not** database object) is deleted (goes down).


Examples
========

Traffic Forwarding
~~~~~~~~~~~~~~~~~~

The following snippet runs the `PostUp` and `PostDown` provided by the unofficial
WireGuard documentation `here <https://github.com/pirate/wireguard-docs#postup>`_
to enable traffic forwarding via iptables rules.

You may append such receivers at the bottom of `models.py` or `admin.py`;
read more about using signals in the `Django Documentation <https://docs.djangoproject.com/en/3.1/topics/signals/>`_

.. danger:: This snippet is **untested** and **unsafe**.

            Please consider using something like `python-iptables <https://github.com/ldx/python-iptables>`_
            instead of non sanitized input fed into `system`.

.. code-block:: python

   from os import system

   from django.dispatch import receiver

   from django_wireguard.signals import interface_created, interface_deleted
   from django_wireguard.models import WireguardInterface

   ...

   # at file bottom

   @receiver(interface_created, sender=WireguardInterface)
   def postup_iptables_route(sender, **kwargs):
      interface: WireguardInterface = kwargs['instance']
      system(f"iptables -A FORWARD -i {interface.name} -j ACCEPT;"
             f"iptables -A FORWARD -o {interface.name} -j ACCEPT;"
             "iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE")

   @receiver(interface_created, sender=WireguardInterface)
   def postdown_iptables_route(sender, instance: 'WireguardInterface', **kwargs):
      interface: WireguardInterface = kwargs['instance']
      system(f"iptables -D FORWARD -i {interface.name} -j ACCEPT;"
             f"iptables -D FORWARD -o {interface.name} -j ACCEPT;"
             "iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE")
